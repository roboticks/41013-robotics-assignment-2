function varargout = GUI_USE(varargin)
% GUI_USE MATLAB code for GUI_USE.fig
%      GUI_USE, by itself, creates a new GUI_USE or raises the existing
%      singleton*.
%
%      H = GUI_USE returns the handle to a new GUI_USE or the handle to
%      the existing singleton*.
%
%      GUI_USE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_USE.M with the given input arguments.
%
%      GUI_USE('Property','Value',...) creates a new GUI_USE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_USE_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are pdoboted to GUI_USE_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_USE

% Last Modified by GUIDE v2.5 16-Oct-2017 11:22:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_USE_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_USE_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_USE is made visible.
function GUI_USE_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_USE (see VARARGIN)
% Choose default command line output for GUI_USE

%Load  object and models
handles.dobot = Assignment2Dobot;
handles.dobot.loadHammer();

% Make edit text boxes  ineditable
redHandle = findobj('Tag','red_remaining');
set(redHandle,'Enable','Off');
greenHandle = findobj('Tag','green_remaining');
set(greenHandle,'Enable','Off');
blueHandle = findobj('Tag','blue_remaining');
set(blueHandle,'Enable','Off');
eStopHandle = findobj('Tag','estop_flag');
set(eStopHandle,'Enable','Inactive');

% Scaling of all gui components
set(findall(hObject, '-property','Units'),'Units','Normalized')

 % Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI_USE wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_USE_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.dobot;


% --- Executes on button press in exit.
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close GUI_USE


% --- Executes on button press in back.
function back_Callback(hObject, eventdata, handles)
% hObject    handle to back (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
GUI
close GUI_USE


function blue_value_Callback(hObject, eventdata, handles)
% hObject    handle to blue_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA
% Hints: get(hObject,'String') returns contents of blue_value as text

% Only postive numbers allowed in the text box
blue = str2num(get(hObject,'String')); %returns contents of blue_value as a integer
if isempty(blue)
    set(hObject,'String','0');
    warndlg('Input must be numerical');
elseif blue < 0
    set(hObject,'String','0');
    warndlg('Input must be positive');    
end

% --- Executes during object creation, after setting all properties.
function blue_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to blue_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function green_value_Callback(hObject, eventdata, handles)
% hObject    handle to green_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of green_value as text

% Only postive numbers allowed in the text box
green = str2num(get(hObject,'String'));% returns contents of green_value as a integer
if isempty(green)
    set(hObject,'String','0');
    warndlg('Input must be numerical');
elseif green < 0
    set(hObject,'String','0');
    warndlg('Input must be positive');    
end


% --- Executes during object creation, after setting all properties.
function green_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to green_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function red_value_Callback(hObject, eventdata, handles)
% hObject    handle to red_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of red_value as text

% Only postive numbers allowed in the text box
red = str2num(get(hObject,'String'));
if isempty(red)
    set(hObject,'String','0');
    warndlg('Input must be numerical');
elseif red < 0
    set(hObject,'String','0');
    warndlg('Input must be positive');    
end


% --- Executes during object creation, after setting all properties.
function red_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to red_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start_candy_sucker.
function start_candy_sucker_Callback(hObject, eventdata, handles)
% hObject    handle to start_candy_sucker (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Grab all user inoput values
redHandle = findobj('Tag', 'red_value');
red = str2double(get(redHandle,'String'));

greenHandle = findobj('Tag', 'green_value');
green = str2double(get(greenHandle,'String'));

blueHandle = findobj('Tag', 'blue_value');
blue = str2double(get(blueHandle,'String'));

% Combine into a single matrix
userWants = [red,green,blue,0,0,0,0,0,0];
%Execute function to pick all candy
handles.dobot.pickAllCandy(userWants);

% Set value back to 0 after  button press
redHandle = findobj('Tag', 'red_value');
red = str2double(set(redHandle,'String',0));

greenHandle = findobj('Tag', 'green_value');
green = str2double(set(greenHandle,'String',0));

blueHandle = findobj('Tag', 'blue_value');
blue = str2double(set(blueHandle,'String',0));

% Update candy remaining textboxes
rgb = (handles.dobot.totalCandy);
rHandle = findobj('Tag','red_remaining');
rValue = (set(rHandle,'String', rgb(1,1)));
gHandle = findobj('Tag','green_remaining');
gValue = (set(gHandle,'String', rgb(1,2)));
bHandle = findobj('Tag','blue_remaining');
bValue = (set(bHandle,'String', rgb(1,3)));


% --- Executes on button press in stahp.
function stahp_Callback(hObject, eventdata, handles)
% hObject    handle to stahp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of stahp

% E-Stop button flag
handles.dobot.estopFlag = get(hObject,'Value');

%If flag is set, change colour and text respective to state of button
if handles.dobot.estopFlag == 1
   estopHandle = findobj('Tag','estop_flag');
   set(estopHandle, 'BackgroundColor', [1 0 0]);
   set(estopHandle, 'String', 'eStop On');
else
   estopHandle = findobj('Tag','estop_flag');
   set(estopHandle, 'BackgroundColor', [0 1 0]);
   set(estopHandle, 'String', 'eStop Off');
end


% --- Executes on button press in collision.
function collision_Callback(hObject, eventdata, handles)
% hObject    handle to collision (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Move d
handles.dobot.moveto(handles.dobot.homePosition);
handles.dobot.moveto(handles.dobot.dropzone * transl([0 0 -0.01]));
handles.dobot.moveto(handles.dobot.collisionPos);


% --- Executes on button press in reset.
function reset_Callback(hObject, eventdata, handles)
% hObject    handle to reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
redHandle = findobj('Tag', 'red_value');
red = str2double(set(redHandle,'String',0));

greenHandle = findobj('Tag', 'green_value');
green = str2double(set(greenHandle,'String',0));

blueHandle = findobj('Tag', 'blue_value');
blue = str2double(set(blueHandle,'String',0));

hammerHandle = findobj('Tag', 'mjolnir_slider');
hammer = (set(hammerHandle,'Value',0.6));
handles.dobot.hammerlocation(3,4) = get(hammerHandle,'Value');
updatedPoints = [handles.dobot.hammerlocation * [handles.dobot.hammerVerts,ones(handles.dobot.hammerVertexCount,1)]']'; 
handles.dobot.hammerMesh.Vertices = updatedPoints(:,1:3);
% hammerTextHandle = findobj('Tag', 'mjolnir_value');
% hammer_text = (set(hammerTextHandle, 'String', 0.525));

% eStopHandle = findobj('Tag', 'stahp');
% eStopVal = set(eStopHandle,'Value', 0);
% handles.dobot.estopFlag = get(eStopHandle,'Value');

handles.dobot.reset;




function red_remaining_Callback(hObject, eventdata, handles)
% hObject    handle to red_remaining (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of red_remaining as text
%        str2double(get(hObject,'String')) returns contents of red_remaining as a double



% --- Executes during object creation, after setting all properties.
function red_remaining_CreateFcn(hObject, eventdata, handles)
% hObject    handle to red_remaining (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function blue_remaining_Callback(hObject, eventdata, handles)
% hObject    handle to blue_remaining (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of blue_remaining as text
%        str2double(get(hObject,'String')) returns contents of blue_remaining as a double



% --- Executes during object creation, after setting all properties.
function blue_remaining_CreateFcn(hObject, eventdata, handles)
% hObject    handle to blue_remaining (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function green_remaining_Callback(hObject, eventdata, handles)
% hObject    handle to green_remaining (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of green_remaining as text
%        str2double(get(hObject,'String')) returns contents of green_remaining as a double



% --- Executes during object creation, after setting all properties.
function green_remaining_CreateFcn(hObject, eventdata, handles)
% hObject    handle to green_remaining (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function mjolnir_slider_Callback(hObject, eventdata, handles)
% hObject    handle to mjolnir_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.dobot.hammerlocation(3,4) = get(hObject,'Value');
updatedPoints = [handles.dobot.hammerlocation * [handles.dobot.hammerVerts,ones(handles.dobot.hammerVertexCount,1)]']'; 
handles.dobot.hammerMesh.Vertices = updatedPoints(:,1:3);
handles.dobot.hammerFlag = 1;

% --- Executes during object creation, after setting all properties.
function mjolnir_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mjolnir_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function estop_flag_Callback(hObject, eventdata, handles)
% hObject    handle to estop_flag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of estop_flag as text
%        str2double(get(hObject,'String')) returns contents of estop_flag as a double



% --- Executes during object creation, after setting all properties.
function estop_flag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to estop_flag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in gfx.
function gfx_Callback(hObject, eventdata, handles)
% hObject    handle to gfx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of gfx
gfxFlag = get(hObject,'Value');
handles.dobot.collisionGFX(gfxFlag);
if gfxFlag == 1
   set(hObject, 'BackgroundColor', [0 1 0]);
else
   set(hObject, 'BackgroundColor', [1 0 0]);
end
