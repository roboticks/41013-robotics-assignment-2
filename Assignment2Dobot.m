classdef Assignment2Dobot < handle %% Class for main code
    properties (Constant)
        base = transl([0 0 0]); %location of base of Dobot robot
        workspace = [-0.2 0.4 -0.4 0.4 -0.1 0.5]; %predefined size of workspace for environment
        homePosition = transl([0.27 0 0.1]); %homing position of end effector after completing function
        collisionPos = transl([0.27 0 0.04]); %forced collision location for robot (inside chute)
    end
    properties
        enclosureLocation;
        traylocation;
        waypoint;
        catcherlocation;
        hammerlocation;
        dropzone;
        dropPoint;
        robot;
        redCandy;
        greenCandy;
        blueCandy;
        totalCandy;
        estopFlag;
        catcherCubePoints;
        trayCubePoints;
        surfaceCubePoints;
        enclosureCubePoints;
        allCubePoints;
        collisionFlag;
        startPoints;
        endPoints;
        prismVertex;
        prismFaces;
        prismFaceNormals;
        hammerFlag;
        hammerVerts;
        hammerVertexCount;
        hammerMesh;
        candyCount;
        ellipsePlot;
        laserPlot;
        pointPlot;
        plotOptions;
    end
    methods
        function self = Assignment2Dobot() %Primary function to load models and set variables 
                    hold on
                    self.robot = Dobot('dobot',self.base,self.workspace);
                    %% Setting Variables and Flags
                    self.enclosureLocation = self.base * transl([0.1 0 -0.03]);
                    self.traylocation = self.base * transl([0.25 0.075 0]);
                    self.waypoint = self.traylocation * transl([0 0 0.1]);
                    self.catcherlocation = self.base * transl([0.25 -0.1 0]);
                    self.dropzone = self.catcherlocation * transl([0 0 0.07]);
                    self.dropPoint = self.catcherlocation * transl([0.07 0 0.01]);
                    self.hammerlocation = self.base * transl([0.1 0 0.6]) * troty(-pi/2);
                    estopLocation = self.base * transl([-0.085 0.3 0.005]); 
                    beacon1Location = self.base * transl([-0.11 -0.22 0.39]) * troty(pi/2); 
                    beacon2Location = self.base * transl([-0.11 0.22 0.39]) * troty(pi/2); 
                    self.collisionFlag = 0;
                    self.hammerFlag = 0;
                    self.estopFlag = 0;
                    
                    %% Load Candy Objects
                    self.loadCandy();
                    %% Load Static Models
                    % load the enclosure 
                    Model('PLY\enclosure.ply', self.enclosureLocation);
                    % load the tray
                    Model('PLY\tray.ply', self.traylocation);
                    % load the catcher
                    Model('PLY\catcher.ply', self.catcherlocation);
                    % load the estop
                    Model('PLY\estop.ply', estopLocation);
                    % load the beacon 1
                    Model('PLY\beacon.ply', beacon1Location);
                    % load the beacon 2
                    Model('PLY\beacon.ply', beacon2Location);
                    
                    %% Collision points for Environment
                    
                    % Collision Points for Catcher
                    
                    % One side of the cube
                    [Y,Z] = meshgrid(-0.04:0.001:0.04,0:0.001:0.07);
                    sizeMat = size(Y);
                    X = repmat(0.04,sizeMat(1),sizeMat(2));

                    % Combine one surface as a point cloud
                    cubePoints = [X(:),Y(:),Z(:)];

                    % Rotate points to make up all faces of catcher
                    cubePoints = [ cubePoints ...
                                 ; cubePoints * rotz(pi/2)...
                                 ; cubePoints * rotz(pi) ...
                                 ; cubePoints * rotz(3*pi/2)];

                    % Translating cube points to desired location
                    self.catcherCubePoints = cubePoints + repmat([0.25,-0.1,0],size(cubePoints,1),1);
                    axis equal
    
                    % Collision points for Tray
                    
                    % One side of the cube
                    [Y,Z] = meshgrid(-0.055:0.001:0.055,0:0.001:0.025);
                    sizeMat = size(Y);
                    X = repmat(0.055,sizeMat(1),sizeMat(2));
                    
                    % Combine one surface as a point cloud
                    cubePoints = [X(:),Y(:),Z(:)];

                    % Rotate points to make faces of Tray
                    cubePoints = [ cubePoints ...
                                 ; cubePoints * rotz(pi/2)...
                                 ; cubePoints * rotz(pi) ...
                                 ; cubePoints * rotz(3*pi/2)];

                    % Translate cube points to tray location
                    self.trayCubePoints = cubePoints + repmat([0.25,0.075,0],size(cubePoints,1),1);
                    axis equal
                    

                    % Collision points for surface
                    
                    % Create mesh grid for surface
                    [Y,X] = meshgrid(-0.3:0.01:0.3,-0.1:0.01:0.3);
                    sizeMat = size(Y);
                    Z = repmat(0,sizeMat(1),sizeMat(2));
                    cubePoints = [X(:),Y(:),Z(:)];                   
                    self.surfaceCubePoints = cubePoints;
                        
                    % Collision for enclosure
                    
                    % Create mesh grid for enclosure
                    [Y,Z] = meshgrid(-0.3:0.01:0.3,0:0.01:0.46);
                    sizeMat = size(Y);
                    X = repmat(-0.11,sizeMat(1),sizeMat(2));
                    cubePoints = [X(:),Y(:),Z(:)];                   
                    self.enclosureCubePoints = cubePoints;
                    
                    % Combine all cube points into one matrix
                    self.allCubePoints = [self.trayCubePoints ; self.catcherCubePoints ; self.surfaceCubePoints ; self.enclosureCubePoints];
                    
                    %% Laser Curtain Points
                    
                    self.startPoints = [0.3 -0.3 0.4; % Front Panel
                                       0.3 -0.3 0.35;
                                       0.3 -0.3 0.3;
                                       0.3 -0.3 0.25;
                                       0.3 -0.3 0.2;
                                       0.3 -0.3 0.15;
                                       0.3 -0.3 0.1;
                                       0.3 -0.3 0.05;
                                       0.3 0.3 0.4; % Right Side Panel
                                       0.3 0.3 0.35;
                                       0.3 0.3 0.3;
                                       0.3 0.3 0.25;
                                       0.3 0.3 0.2;
                                       0.3 0.3 0.15;
                                       0.3 0.3 0.1;
                                       0.3 0.3 0.05; 
                                       0.3 -0.3 0.4; % Left Side Panel
                                       0.3 -0.3 0.35;
                                       0.3 -0.3 0.3;
                                       0.3 -0.3 0.25;
                                       0.3 -0.3 0.2;
                                       0.3 -0.3 0.15;
                                       0.3 -0.3 0.1;
                                       0.3 -0.3 0.05;
                                       0.25 -0.3 0.46; % Top Panel
                                       0.2 -0.3 0.46;
                                       0.15 -0.3 0.46;
                                       0.1 -0.3 0.46;
                                       0.05 -0.3 0.46;
                                       0 -0.3 0.46;
                                       -0.05 -0.3 0.46];
                    self.endPoints = [0.3 0.3 0.4; % Front Panel
                                       0.3 0.3 0.35;
                                       0.3 0.3 0.3;
                                       0.3 0.3 0.25;
                                       0.3 0.3 0.2;
                                       0.3 0.3 0.15;
                                       0.3 0.3 0.1;
                                       0.3 0.3 0.05;
                                       -0.1 0.3 0.4; % Right Side Panel
                                       -0.1 0.3 0.35;
                                       -0.1 0.3 0.3;
                                       -0.1 0.3 0.25;
                                       -0.1 0.3 0.2;
                                       -0.1 0.3 0.15;
                                       -0.1 0.3 0.1;
                                       -0.1 0.3 0.05;
                                       -0.1 -0.3 0.4; % Left Side Panel
                                       -0.1 -0.3 0.35;
                                       -0.1 -0.3 0.3;
                                       -0.1 -0.3 0.25;
                                       -0.1 -0.3 0.2;
                                       -0.1 -0.3 0.15;
                                       -0.1 -0.3 0.1;
                                       -0.1 -0.3 0.05;
                                       0.25 0.3 0.46; % Top Panel
                                       0.2 0.3 0.46;
                                       0.15 0.3 0.46;
                                       0.1 0.3 0.46;
                                       0.05 0.3 0.46;
                                       0 0.3 0.46;
                                       -0.05 0.3 0.46];                  
        end        
        function loadCandy(self) %Load candy models and locations relative to the tray
            self.totalCandy = [4,4,4,4,4,4,4,4,4]; %Initial candy stock in each tray cell
            self.redCandy = Candy(4,1,'red',self.traylocation);
            self.greenCandy = Candy(4,2,'green',self.traylocation);
            self.blueCandy = Candy(4,3,'blue',self.traylocation);
            % yellowCandy = Candy(5,4,'yellow',traylocation);
            % cyanCandy = Candy(5,5,'cyan',traylocation);
            % magentaCandy = Candy(5,6,'magenta',traylocation);
            % blackCandy = Candy(5,7,'black',traylocation);
            % whiteCandy = Candy(5,8,'white',traylocation);
            % greyCandy = Candy(5,9,'grey',traylocation);
        end
         
        
        function PickAllCandy(self,userWants) %Function executed when "Suck Candy" button is pressed to pick up desired candy      
            candyRemaining = self.totalCandy - userWants 
            for i=1:1:length(candyRemaining) %If user inputs higher value than candy stock, show dialogue and exit function
                if 0 > candyRemaining(1,i)
                    warndlg('Not enough candy. Eat less sugar.');
                    return;
                end
            end
            
            candyRemaining = candyRemaining + ones(1,9); %Increment candyRemaining so that the correct candy is picked up
            
            self.candyCount = 0; %counter for current number of candy picked up in a cycle to update the drop position 
            
            % Execute CandyPickup function to pick up each candy
            self.CandyPickup(self.redCandy, self.totalCandy(1,1), candyRemaining(1,1));
            self.CandyPickup(self.greenCandy, self.totalCandy(1,2), candyRemaining(1,2));
            self.CandyPickup(self.blueCandy, self.totalCandy(1,3), candyRemaining(1,3));
            self.MoveTo(self.homePosition);
            self.candyCount = 0; % Reset candyCount after all Candy has been moved
            
            % Remove all Candy in the chute
            self.redCandy.RemoveEaten();
            self.greenCandy.RemoveEaten();
            self.blueCandy.RemoveEaten();
            
            % Update totalCandy that is left in stock
            self.totalCandy = self.totalCandy - userWants;
        end
        
        function CandyPickup(self, candy, totalCandy, candyRemaining)    %Function to pickup desired amount of candy
            for i = totalCandy:-1:candyRemaining
                self.MoveTo(self.waypoint);
                self.MoveTo(candy.model{i}.base);
                self.MoveTo(self.waypoint, candy.model{i});
                self.MoveTo(self.dropzone, candy.model{i});
                Drop(candy.model{i}, self.candyCount); %Drop function to animate candy models being dropped and stacked
                self.candyCount = self.candyCount + 1;
            end
        end
        
        function MoveTo(self, endpos, candy) %Function to move robot to desired location and candy if conditions are met
            steps = 50;
            startQ = self.robot.model.getpos(); % Get current joint angles
            endPosition = endpos; % Target end effector position
            qGuess = [0,pi/4,pi/2,pi/4,0]; % Guessed joint positions for ikcon
            newQ = self.robot.model.ikcon(endPosition,qGuess); % Updated joint position for desired end effector location
            qMatrix = TrapProfile(startQ,newQ,steps); % Use Trapezoidal Profile Method to obtain qMatrix
            
            for trajStep = 1:size(qMatrix,1)
                        
                % Estop Check
                if self.estopFlag ~= 0
                    warndlg('EMERGENCY STAHP ACTIVATED!')
                    while self.estopFlag ~=0
                        pause(0.1);
                    end
                end

                %If hammer has been moved, update the collision
                %rectangle, and check for line plane intersection
                %collision with laser curtain
                if self.hammerFlag == 1
                    % Update Position Rectangle 
                    centerpnt = self.hammerlocation(1:3,4)';
                    prismSize = [0.15 0.09 0.11];
                    [self.prismVertex,self.prismFaces,self.prismFaceNormals] = RectangularPrism(centerpnt-prismSize/2, centerpnt+prismSize/2,self.plotOptions);   
                    % Laser curtain Collision Check
                    isObjectCollision = ObjectCollisionCheck(self.startPoints,self.endPoints,self.prismVertex,self.prismFaces,self.prismFaceNormals);
                    if isObjectCollision == 1
                        display('Object Collision Detected!');
                        while isObjectCollision == 1 %Pause model movement until collision is no longer detected
                            pause(0.1);
                            centerpnt = self.hammerlocation(1:3,4)';
                            prismSize = [0.15 0.09 0.11];
                            [self.prismVertex,self.prismFaces,self.prismFaceNormals] = RectangularPrism(centerpnt-prismSize/2, centerpnt+prismSize/2,self.plotOptions);   
                            isObjectCollision = ObjectCollisionCheck(self.startPoints,self.endPoints,self.prismVertex,self.prismFaces,self.prismFaceNormals);
                        end
                    end

                    self.hammerFlag = 0; % Reset hammer movement flag
                end

                % Recalculate Q4 based on Q2 & Q3
                robotQ = self.robot.model.getpos();
                robotQ2 = robotQ(2);
                robotQ3 = robotQ(3);
                robotQ4 = pi/2 - robotQ2 - robotQ3;
                q = qMatrix(trajStep,:);
                q(1,4) = robotQ4;

                % Robot Ellipsoid Collision Check
                isCollisionCatcher = CollisionCheck(self.catcherCubePoints, self.robot, q);
                isCollisionTray = CollisionCheck(self.trayCubePoints, self.robot, q);
                isCollisionSurface = CollisionCheck(self.surfaceCubePoints, self.robot, q);
                isCollisionEnclosure = CollisionCheck(self.enclosureCubePoints, self.robot, q);
                if isCollisionCatcher == 1 || isCollisionTray == 1 || isCollisionSurface == 1 || isCollisionEnclosure == 1 % If in collision pause until robot is reset
                    self.collisionFlag = 1;
                    while self.collisionFlag == 1
                        pause(0.1);
                        if self.collisionFlag == 0
                            return;
                        end
                    end
                end

                % Pickup Candy Check - if the number of arguments
                % are greater than 2 this IF statement is activated
                % and updates the candy model along with the
                % endeffector of the robot.
                if nargin > 2
                    tr = self.robot.model.fkine(q);
                    candy.base = tr;
                    candy.animate(0);
                end

                % Update and draw robot
                self.robot.model.animate(q);
                drawnow();
            end
        end
        
        function UpdateJoint(self, joint, angle) %Function executed for GUI_TEACH joint slider movement
            
            % Do not update if robot is in collision
            if self.collisionFlag == 1
                return;
            end
            
            % Update joints and recalculate Q4
            qNew = self.robot.model.getpos();
            qNew(1,joint) = deg2rad(angle);
            q4 = pi/2 - qNew(1,2) -qNew(1,3);
            qNew(1,4) = q4;
            
            % Check for robot collision with environment
            isCollisionCatcher = CollisionCheck(self.catcherCubePoints, self.robot, qNew);
            isCollisionTray = CollisionCheck(self.trayCubePoints, self.robot, qNew);
            isCollisionSurface = CollisionCheck(self.surfaceCubePoints, self.robot, qNew);
            isCollisionEnclosure = CollisionCheck(self.enclosureCubePoints, self.robot, qNew);
            % If in collision do not animate, otherwise animate.
            if isCollisionCatcher == 1 || isCollisionTray == 1 || isCollisionSurface == 1 || isCollisionEnclosure == 1
                self.collisionFlag = 1;
                return;            
            else
                self.robot.model.animate(qNew);
                drawnow();  
            end            
        end
        
        function Reset(self) %Fucntion for resetting the robot in both GUI_USE and GUI_TEACH
            self.collisionFlag = 0;
            self.robot.model.animate(self.robot.qDefault);
            drawnow();
        end
        
        function XYZ = GetCurrentPos(self) %Function to update robot based on user input end effector location for GUI_TEACH
           qCurrent = self.robot.model.getpos(); 
           XYZ = self.robot.model.fkine(qCurrent);
           XYZ = XYZ(1:3,4);
        end
        
        function LoadHammer(self) % Load hammer model and collision rectangle for only GUI_USE
            % Load the hammer
            % Load the triangle mesh
            [f,v,data] = plyread('PLY\hammer.ply','tri');
            % Get vertex count
            self.hammerVertexCount = size(v,1);

            self.hammerVerts = v;

            % Create a transform to describe the location
            hammerPose = self.hammerlocation;

            % Scale the colours to be 0-to-1 (they are originally 0-to-255)
            vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

            % Then plot the trisurf
            self.hammerMesh = trisurf(f,self.hammerVerts(:,1),self.hammerVerts(:,2), self.hammerVerts(:,3) ...
            ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

            % Transform the vertices
            updatedPoints = [hammerPose * [self.hammerVerts,ones(self.hammerVertexCount,1)]']'; 

            % Update the mesh vertices in the patch handle
            self.hammerMesh.Vertices = updatedPoints(:,1:3);

            % Plot Collision Rectangle 
            centerpnt = self.hammerlocation(1:3,4)';
            prismSize = [0.15 0.09 0.11];
            self.plotOptions.plotFaces = false;
            [self.prismVertex,self.prismFaces,self.prismFaceNormals] = RectangularPrism(centerpnt-prismSize/2, centerpnt+prismSize/2,self.plotOptions);
            axis equal;
            camlight;
        end
        
        function CollisionGFX(self, plot) %Function to plot laser curtain, collision points and ellipsoid approximation of robot
            
            if plot == 1     
                % Plot Collision Ellipses
                for i = 1:6
                    [X,Y,Z] = ellipsoid( self.robot.ellipseCenterPoint(i,1), self.robot.ellipseCenterPoint(i,2), self.robot.ellipseCenterPoint(i,3), self.robot.ellipseRadii(i,1), self.robot.ellipseRadii(i,2), self.robot.ellipseRadii(i,3) )
                    self.robot.model.points{i} = [X(:),Y(:),Z(:)];
                    warning off;
                    self.robot.model.faces{i} = delaunay(self.robot.model.points{i});
                    warning on;
                end
                self.robot.model.plot3d(self.robot.model.getpos());

                % Plot Laser Curtain
                for i=1:size(self.startPoints,1)
                    self.laserPlot(1,i) = plot3([self.startPoints(i,1),self.endPoints(i,1)],[self.startPoints(i,2),self.endPoints(i,2)],[self.startPoints(i,3),self.endPoints(i,3)],'r');
                end
                
                % Plot Collision Points
                self.pointPlot = plot3(self.allCubePoints(:,1),self.allCubePoints(:,2),self.allCubePoints(:,3),'g.');
                
            else % Try to delete plots
                try
                    delete(self.laserPlot);
                end
                try
                    delete(self.pointPlot);
                end
            end
        end
    end
end
