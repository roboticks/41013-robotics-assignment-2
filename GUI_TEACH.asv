function varargout = GUI_TEACH(varargin)
% GUI_TEACH MATLAB code for GUI_TEACH.fig
%      GUI_TEACH, by itself, creates a new GUI_TEACH or raises the existing
%      singleton*.
%
%      H = GUI_TEACH returns the handle to a new GUI_TEACH or the handle to
%      the existing singleton*.
%
%      GUI_TEACH('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_TEACH.M with the given input arguments.
%
%      GUI_TEACH('Property','Value',...) creates a new GUI_TEACH or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_TEACH_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are pdoboted to GUI_TEACH_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_TEACH

% Last Modified by GUIDE v2.5 15-Oct-2017 22:27:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_TEACH_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_TEACH_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_TEACH is made visible.
function GUI_TEACH_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_TEACH (see VARARGIN)

% Choose default command line output for GUI_TEACH

%Load  object and models
handles.dobot = Assignment2Dobot;

% Make edit text boxes uneditable
q1Handle = findobj('Tag','q1_text');
set(q1Handle,'Enable','Off');
q2Handle = findobj('Tag','q2_text');
set(q2Handle,'Enable','Off');
q3Handle = findobj('Tag','q3_text');
set(q3Handle,'Enable','Off');
q5Handle = findobj('Tag','q5_text');
set(q5Handle,'Enable','Off');

% Round off numbers to nearest 2 decimals in edit text boxes and display
% values based on current robot position
xyz = round((handles.dobot.currentPos),2);
xHandle = findobj('Tag','x_value');
xValue = (set(xHandle,'String', xyz(1,1)));
yHandle = findobj('Tag','y_value');
yValue = (set(yHandle,'String', xyz(2,1)));
zHandle = findobj('Tag','z_value');
zValue = (set(zHandle,'String', xyz(3,1)));

% Scaling of all gui components
set(findall(hObject, '-property','Units'),'Units','Normalized')

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI_TEACH wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_TEACH_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.dobot;


% --- Executes on button press in back.
function back_Callback(hObject, eventdata, handles)
% hObject    handle to back (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
GUI
close GUI_TEACH


% --- Executes on button press in exit.
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close GUI_TEACH


% --- Executes on slider movement.
function q5_value_Callback(hObject, eventdata, handles)
% hObject    handle to q5_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Round off numbers to nearest 2 decimals in edit text boxes and display
% values based on current robot position
xyz = round((handles.dobot.currentPos),2);
xHandle = findobj('Tag','x_value');
xValue = (set(xHandle,'String', xyz(1,1)));
yHandle = findobj('Tag','y_value');
yValue = (set(yHandle,'String', xyz(2,1)));
zHandle = findobj('Tag','z_value');
zValue = (set(zHandle,'String', xyz(3,1)));

% Grab value form user input and update robot model
q5 = (get(hObject,'Value'));
qHandle = findobj('Tag', 'q5_text');
q = (set(qHandle,'String', q5));
handles.dobot.updateJoint(5, q5)


% --- Executes during object creation, after setting all properties.
function q5_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q5_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q3_value_Callback(hObject, eventdata, handles)
% hObject    handle to q3_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Round off numbers to nearest 2 decimals in edit text boxes and display
% values based on current robot position
xyz = round((handles.dobot.currentPos),2);
xHandle = findobj('Tag','x_value');
xValue = (set(xHandle,'String', xyz(1,1)));
yHandle = findobj('Tag','y_value');
yValue = (set(yHandle,'String', xyz(2,1)));
zHandle = findobj('Tag','z_value');
zValue = (set(zHandle,'String', xyz(3,1)));

% Grab value form user input and update robot model
q3 = (get(hObject,'Value'));
qHandle = findobj('Tag', 'q3_text');
q = (set(qHandle,'String', q3));
handles.dobot.updateJoint(3, q3)


% --- Executes during object creation, after setting all properties.
function q3_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q3_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q2_value_Callback(hObject, eventdata, handles)
% hObject    handle to q2_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Round off numbers to nearest 2 decimals in edit text boxes and display
% values based on current robot position
xyz = round((handles.dobot.currentPos),2);
xHandle = findobj('Tag','x_value');
xValue = (set(xHandle,'String', xyz(1,1)));
yHandle = findobj('Tag','y_value');
yValue = (set(yHandle,'String', xyz(2,1)));
zHandle = findobj('Tag','z_value');
zValue = (set(zHandle,'String', xyz(3,1)));

% Grab value form user input and update robot model
q2 = (get(hObject,'Value'));
qHandle = findobj('Tag', 'q2_text');
q = (set(qHandle,'String', q2));
handles.dobot.updateJoint(2, q2)


% --- Executes during object creation, after setting all properties.
function q2_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q2_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q1_value_Callback(hObject, eventdata, handles)
% hObject    handle to q1_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Round off numbers to nearest 2 decimals in edit text boxes and display
% values based on current robot position
xyz = round((handles.dobot.currentPos),2);
xHandle = findobj('Tag','x_value');
xValue = (set(xHandle,'String', xyz(1,1)));
yHandle = findobj('Tag','y_value');
yValue = (set(yHandle,'String', xyz(2,1)));
zHandle = findobj('Tag','z_value');
zValue = (set(zHandle,'String', xyz(3,1)));

% Grab value form user input and update robot model
q1 = (get(hObject,'Value'));
qHandle = findobj('Tag', 'q1_text');
q = (set(qHandle,'String', q1));
handles.dobot.updateJoint(1, q1)


% --- Executes during object creation, after setting all properties.
function q1_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q1_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function z_value_Callback(hObject, eventdata, handles)
% hObject    handle to z_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of z_value as text
%        str2double(get(hObject,'String')) returns contents of z_value as a double

% If user input value not within limits, show warning
zValue = str2num(get(hObject,'String'));
if isempty(zValue)
    set(hObject,'String','0');
    warndlg('Input must be numerical');
elseif zValue < 0 || zValue > 0.45
    set(hObject,'String','0');
    warndlg('Dobot too small to reach!');    
end

% --- Executes during object creation, after setting all properties.
function z_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to z_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y_value_Callback(hObject, eventdata, handles)
% hObject    handle to y_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of y_value as text
%        str2double(get(hObject,'String')) returns contents of y_value as a double

% If user input value not within limits, show warning
yValue = str2num(get(hObject,'String'));
if isempty(yValue)
    set(hObject,'String','0');
    warndlg('Input must be numerical');
elseif yValue < -0.3 || yValue > 0.3
    set(hObject,'String','0');
    warndlg('Dobot too small to reach!');    
end

% --- Executes during object creation, after setting all properties.
function y_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x_value_Callback(hObject, eventdata, handles)
% hObject    handle to x_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of x_value as text
%        str2double(get(hObject,'String')) returns contents of x_value as a double

% If user input value not within limits, show warning
xValue = str2num(get(hObject,'String'));
if isempty(xValue)
    set(hObject,'String','0');
    warndlg('Input must be numerical');
elseif xValue < -0.1 || xValue > 0.4
    set(hObject,'String','0');
    warndlg('Dobot too small to reach!');    
end

% --- Executes during object creation, after setting all properties.
function x_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in move.
function move_Callback(hObject, eventdata, handles)
% hObject    handle to move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%the move_Callback fetches all the numbers that were input by the user in
%the X Y Z text boxes, it then dobotigns theem to their respectible variable
%which gets convereted to 4*4 homogeneous matrix and pushed into the moveto
%function that is in the Assingment2Dobot.m file.
xHandle = findobj('Tag', 'x_value');
x = str2double(get(xHandle,'String'));

yHandle = findobj('Tag', 'y_value');
y = str2double(get(yHandle,'String'));

zHandle = findobj('Tag', 'z_value');
z = str2double(get(zHandle,'String'));

Coordinate = transl([x y z]);
handles.dobot.moveto(Coordinate);


% --- Executes on button press in reset.
function reset_Callback(hObject, eventdata, handles)
% hObject    handle to reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
q1Handle = findobj('Tag', 'q1_value');
q1 = (set(q1Handle,'Value',0));
q1TextHandle = findobj('Tag', 'q1_text');
q1_text = (set(q1TextHandle, 'String', 0.0));

q2Handle = findobj('Tag', 'q2_value');
q2 = (set(q2Handle,'Value',45));
q2TextHandle = findobj('Tag', 'q2_text');
q2_text = (set(q2TextHandle, 'String', 45.0));

q3Handle = findobj('Tag', 'q3_value');
q3 = (set(q3Handle,'Value',90));
q3TextHandle = findobj('Tag', 'q3_text');
q3_text = (set(q3TextHandle, 'String', 92.5));

q5Handle = findobj('Tag', 'q5_value');
q5 = (set(q5Handle,'Value',0));
q5TextHandle = findobj('Tag', 'q5_text');
q5_text = (set(q5TextHandle, 'String', 0.0));

handles.dobot.reset;

xyz = round((handles.dobot.currentPos),2);
xHandle = findobj('Tag','x_value');
xValue = (set(xHandle,'String', xyz(1,1)));
yHandle = findobj('Tag','y_value');
yValue = (set(yHandle,'String', xyz(2,1)));
zHandle = findobj('Tag','z_value');
zValue = (set(zHandle,'String', xyz(3,1)));



function q5_text_Callback(hObject, eventdata, handles)
% hObject    handle to q5_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q5_text as text
%        str2double(get(hObject,'String')) returns contents of q5_text as a double
text = str2double(get(hObject, 'String'));
qHandle = findobj('Tag', 'q5_value');
q = (set(qHandle,'Value', text));


% --- Executes during object creation, after setting all properties.
function q5_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q5_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q3_text_Callback(hObject, eventdata, handles)
% hObject    handle to q3_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q3_text as text
%        str2double(get(hObject,'String')) returns contents of q3_text as a double
text = str2double(get(hObject, 'String'));
qHandle = findobj('Tag', 'q3_value');
q = (set(qHandle,'Value', text));


% --- Executes during object creation, after setting all properties.
function q3_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q3_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q2_text_Callback(hObject, eventdata, handles)
% hObject    handle to q2_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q2_text as text
%        str2double(get(hObject,'String')) returns contents of q2_text as a double
text = str2double(get(hObject, 'String'));
qHandle = findobj('Tag', 'q2_value');
q = (set(qHandle,'Value', text));


% --- Executes during object creation, after setting all properties.
function q2_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q2_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q1_text_Callback(hObject, eventdata, handles)
% hObject    handle to q1_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q1_text as text
%        str2double(get(hObject,'String')) returns contents of q1_text as a double
text = str2double(get(hObject, 'String'));
qHandle = findobj('Tag', 'q1_value');
q = (set(qHandle,'Value', text));



% --- Executes during object creation, after setting all properties.
function q1_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q1_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

