function Model(name, transform) % Load ply models in desired location and orientation
[f,v,data] = plyread(name,'tri');

Count = size(v,1);

XYZ = v;

vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

Mesh = trisurf(f,XYZ(:,1),XYZ(:,2), XYZ(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

newloc = [ transform * [XYZ,ones(Count,1)]']'; 
Mesh.Vertices = newloc(:,1:3);

end
