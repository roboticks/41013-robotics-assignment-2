function Drop(candy, count) %Drop function to animate candy models being dropped and stacked
    % Points for candy dropping animation
    dropAnimate = [0.25 -0.1 0.07;
                   0.254 -0.1 0.048;
                   0.266 -0.1 0.03;
                   0.281 -0.1 0.018;
                   0.3 -0.1 0.012;
                   0.32 -0.1 0.01];
    % Animate candy dropping
    for i=2:size(dropAnimate,1)-1
        candy.base = transl(dropAnimate(i,:));
        candy.animate(0);
        drawnow();
    end
    % Stack candy on top of drop point
    candy.base = transl(dropAnimate(6,:)) * transl([0 0 count*0.005]);
    candy.animate(0);
    drawnow();
end    