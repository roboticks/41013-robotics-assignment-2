function inCollision = CollisionCheck(cubePoints, robot, q) %Ellipsoidal Collision check with environment collision points

                        % Collision Check with end effector ellipsoid only                       
                        trCubePoint = robot.model.fkine(q);
                        cubePointsAndOnes = [inv(trCubePoint) * [cubePoints,ones(size(cubePoints,1),1)]']';
                        updatedCubePoints = cubePointsAndOnes(:,1:3);
                        algebraicDist = GetAlgebraicDist(updatedCubePoints, robot.ellipseCenterPoint(6,:), robot.ellipseRadii(6,:));
                        pointsInside = find(algebraicDist < 1);

                        if pointsInside > 0 %if collision points are detected within ellipsoid then return a collision
                            display('Collision Detected!');
                            inCollision = 1;
                        else
                            inCollision = 0;
                        end
                        
%                         % Collision Check with all joint ellipsoids
%                         % CubePoint Translations
%                         trCubePoint = zeros(4,4,self.robot.model.n+1);
%                         trCubePoint(:,:,1) = self.robot.base;
%                         L = self.robot.model.links;
%                         for i = 1 : self.robot.model.n
%                             trCubePoint(:,:,i+1) = trCubePoint(:,:,i) * trotz(q(i)) * transl(0,0,L(i).d) * transl(L(i).a,0,0) * trotz(L(i).alpha);
%                         end
%                         
%                         % Go through each Ellipsoid
%                         
%                         for i = 6: size(trCubePoint,3)
% %                             cubePointsAndOnes = [inv(trCubePoint(:,:,i)) * [self.catcherCubePoints,ones(size(self.catcherCubePoints,1),1)]']';
%                             cubePointsAndOnes = [inv(trCubePoint(:,:,i)) * [self.catcherCubePoints,ones(size(self.catcherCubePoints,1),1)]']';
%                             updatedCubePoints = cubePointsAndOnes(:,1:3)
%                             cube_h = plot3(updatedCubePoints(:,1),updatedCubePoints(:,2),updatedCubePoints(:,3),'b.');
%                             algebraicDist = GetAlgebraicDist(updatedCubePoints, self.robot.ellipseCenterPoint(i,:), self.robot.ellipseRadii(i,:));
%                             pointsInside = find(algebraicDist < 1);
%                             display(['There are ', num2str(size(pointsInside,1)),' points inside the ',num2str(i),'th ellipsoid']);
%                         end
end
