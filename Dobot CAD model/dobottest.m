

L1 = Link('d',0.082,'a',0,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-135), deg2rad(135)]); % BaseAngle = q1
L2 = Link('d',0,'a',0.135,'alpha',0,'offset',-pi/2,'qlim',[deg2rad(5), deg2rad(80)]); % RearArmAngle = q2
L3 = Link('d',0,'a',0.160,'alpha',0,'offset',0,'qlim',[deg2rad(15), deg2rad(170)]); % ForeArmAngle = q3 - pi/2 + q2, q3 = pi/2 - q2 + ForeArmAngle
L4 = Link('d',0,'a',0.05254,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-90), deg2rad(90)]); % N/A (cannot be controlled)
L5 = Link('d',0.06782,'a',0,'alpha',pi,'offset',0,'qlim',[deg2rad(-85), deg2rad(85)]); % ServoAngle = q5

robot = SerialLink([L1 L2 L3 L4 L5],'name','dobot')

workspace = [-0.2 0.4 -0.4 0.4 -0.1 0.4];
scale = 0.5;
q = [0,pi/4,pi/2,-pi/4,0] 
robot.plot(q,'workspace',workspace,'scale',scale);
robot.teach;