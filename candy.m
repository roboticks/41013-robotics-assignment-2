classdef Candy < handle 
    properties
        count;
        model;
        filename;
        faceData = [];
        vertexData = [];
        plyData = [];q
        cell;
        eatenState;
    end
    
    methods
        function self = Candy(count,cell,filename,traylocation)
            if 0 < nargin
                self.count = count;
            end
            self.eatenState = zeros(1,self.count); % Initialise all candy not eaten
            trayThickness = 0.005; %Thickness of tray base
            candyHeight = 0.005 ; %Height of each candy
            % Create the required number of candy
            for i = 1:self.count
                self.model{i} = self.GetModel([filename,num2str(i)],filename);
                % Candy plot location based on swtich statement selected
                switch(cell)
                    case 1
                        self.model{i}.base = traylocation * transl([-0.035 -0.035 trayThickness+i*candyHeight]);
                    case 2
                        self.model{i}.base = traylocation * transl([-0.035 0.0 trayThickness+i*candyHeight]);
                    case 3
                        self.model{i}.base = traylocation * transl([-0.035 0.035 trayThickness+i*candyHeight]);
                    case 4
                        self.model{i}.base = traylocation * transl([0.0 -0.035 trayThickness+i*candyHeight]);
                    case 5
                        self.model{i}.base = traylocation * transl([0.0 0.0 trayThickness+i*candyHeight]);
                    case 6
                        self.model{i}.base = traylocation * transl([0.0 0.035 trayThickness+i*candyHeight]);
                    case 7
                        self.model{i}.base = traylocation * transl([0.035 -0.035 trayThickness+i*candyHeight]);
                    case 8
                        self.model{i}.base = traylocation * transl([0.035 0.0 trayThickness+i*candyHeight]);
                    case 9
                        self.model{i}.base = traylocation * transl([0.035 0.035 trayThickness+i*candyHeight]);
                end
                 % Plot 3D model
                 self.model{i}.animate(0);
                % Hold on after the first plot (if already on there's no difference)
                if i == 1 
                    hold on;
                end
            end
            axis equal
            camlight;
        end    
        %% Remove taken candy
        function RemoveEaten(self)
            for index = 1:self.count
                if self.eatenState(index) == 0 & self.model{index}.base(1:2,4)' == [0.32 -0.1]
                    handles = findobj('Tag', self.model{index}.name);
                    h = get(handles,'UserData');
                    
                    % Then make candy invisible
                    h.link(1).Children.FaceColor = 'none';                    
                    h.link(1).Children.FaceVertexCData = [];
                    h.link(1).Children.CData = [];                    
                    h.link(1).Children.XData = h.link(1).Children.XData * eps;
                    h.link(1).Children.YData = h.link(1).Children.YData * eps;
                    h.link(1).Children.ZData = h.link(1).Children.ZData * eps; 
                    
                    % Don't try and remove again
                    self.eatenState(index) = 1;
                    animate(self.model{index},0);
                end
            end
        end   
    
        %% Get and plot the candy models
        function model = GetModel(self,name,filename)
            if nargin < 1
                name = 'Candy';
            end
            if isempty(self.faceData) || isempty(self.vertexData) || isempty(self.plyData)
                [self.faceData,self.vertexData,self.plyData] = plyread(['PLY\',filename,'.ply'],'tri');
            end
            L1 = Link('alpha',0,'a',1,'d',0,'offset',0);
            model = SerialLink(L1,'name',name);
            model.faces = {self.faceData,[]};
            model.points = {self.vertexData,[]};
                                   
            plot3d(model,0,'workspace',[-0.2 0.4 -0.4 0.4 -0.1 0.5],'delay',0);
            handles = findobj('Tag', model.name);
            h = get(handles,'UserData');
            h.link(1).Children.FaceVertexCData = [self.plyData.vertex.red ...
                                                 ,self.plyData.vertex.green ...
                                                 ,self.plyData.vertex.blue]/255;
            h.link(1).Children.FaceColor = 'interp';
        end
    end    
end

