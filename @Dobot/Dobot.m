classdef Dobot < handle %% Class for Dobot robot model
    properties
        model;
        base;
        name;
        workspace; 
        qDefault;
        ellipseCenterPoint;
        ellipseRadii;
    end
    
    methods
        function self = Dobot(name,base,workspace)   
        self.base = base; % Base parameter in 4x4 homog transform
        self.name = name; % Name of robot in string
        self.workspace = workspace; % Define the boundaries of the workspace
        self.qDefault = [0,pi/4,pi/2,-pi/4,0];    
        self.GetDobotRobot(); 
        self.GetCollisionEllipses();
        self.PlotAndColourRobot();
        end
        %% Create and return Dobot Robot with a given name and base position
        function GetDobotRobot(self)

        L1 = Link('d',0.104,'a',0,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-135), deg2rad(135)]); % BaseAngle = q1
        L2 = Link('d',0,'a',0.135,'alpha',0,'offset',-pi/2,'qlim',[deg2rad(5), deg2rad(80)]); % RearArmAngle = q2
        L3 = Link('d',0,'a',0.160,'alpha',0,'offset',0,'qlim',[deg2rad(15), deg2rad(170)]); % ForeArmAngle = q3 - pi/2 + q2, q3 = pi/2 - q2 + ForeArmAngle
        L4 = Link('d',0,'a',0.05254,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-90), deg2rad(90)]); % N/A (cannot be controlled)
        L5 = Link('d',0.06782,'a',0,'alpha',pi,'offset',0,'qlim',[deg2rad(-85), deg2rad(85)]); % ServoAngle = q5

            self.model = SerialLink([L1 L2 L3 L4 L5],'name',self.name,'base',self.base);   
        end
        %% Create Collision Ellipses for each Dobot Joint
        function GetCollisionEllipses(self)
            % Ellipse CentrePoints and Radii determined to approximate each
            % robot joint
            self.ellipseCenterPoint = [0 0 0;
                                       0 0.05 0;
                                       -0.07 -0.02 0;
                                       -0.08 -0.025 0;
                                       -0.02 0 0;
                                       0 0 0.03];
            self.ellipseRadii = [0.1 0.08 0.03;
                                 0.04 0.08 0.04;
                                 0.09 0.03 0.02;
                                 0.09 0.025 0.02;
                                 0.035 0.01 0.02;
                                 0.009 0.009 0.03];

        end
        %% PlotAndColourRobot
        % Given a robot index, add the vertices and faces and colour them in if data is available
        function PlotAndColourRobot(self)
            for linkIndex = 0:self.model.n
                [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['j',num2str(linkIndex),'.ply'],'tri');
                self.model.faces{linkIndex+1} = faceData;
                self.model.points{linkIndex+1} = vertexData;
            end

            % Display robot
            self.model.plot3d(self.qDefault,'noarrow','workspace',self.workspace,'fps',60);
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight
            end  
            self.model.delay = 0;

            % Colouring the Robot
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try 
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                                  , plyData{linkIndex+1}.vertex.green ...
                                                                  , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end        
    end
end